// Operators
/*

	Add + = Sum
	Subtract - = Difference
	Multiply * = Product
	Divide / = Quotient
	Modulus % = Remainder
*/

function mod(){
	return 9 % 2;
}

console.log(mod());

// 8 * (6 + 2) - (2 * 3)

// Assingment operatoor (=)


/*
	+= (Addition)
	-= (Subtraction)
	*= (Multiplication)
	/= (Division)
	%= (Modulo)

*/

    let x = 1;

	let sum = 1;
	// sum = sum + 1;
	sum += 1

	console.log(sum)

// Increment and Decrement (++, --)
//  operators that add or subtract values by 1 an reassigns the value of the variable where the increment/decrement was applied to 



let z = 1;

// Pre-increment
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// Post-increment 
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

// Pre-decrement 

let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

//Post-decrement 

decrement = z--;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// Comparison Operators

// boolean value= "true or false"

// Equality Operator (==)

let juan = 'Juan';

console.log(1 == 1);
console.log(0 == false);
console.log('Juan' == juan);

// Strict equality (===)
console.log(1 === true);

// Inequality Operator(!=)
console.log("Inequality Operator");
console.log(1 != 1);
console.log('juan' != juan)

// Strict inequality (!==)
console.log(0 !== false);

let	islegalAge = true
let	isResgistered = false


// And Operator
let allRequirementsMet = islegalAge && isResgistered;
console.log("Result of ligical AND operator: " + allRequirementsMet);

// Or Operator
let someRequirementsMet = islegalAge || isResgistered;
console.log("Result of ligical AND operator: " + someRequirementsMet);

// selection control structure

/* IF statement
	- execute a statement if a specified condition is true
	syntax
	if(condition{
		statement/s;
	})
*/

let	num = -1

if(num < 0){
	console.log('Hello');
}

// Creat an if statement that will display the words "Welcome to Zuitt" if a certain value is greater than or equal to 10


let num2 = 10

if(num2 >= 10){
	console.log('Welcome to Zuitt')
}

num = 2
if(num >= 10){
	console.log('Welcome to Zuitt')
}

/*	IF-ELSE statement
		- execute a statement if the previous condition returns false.

		syntax:
		if(condition){
			Statement/s;
		}
		else{
			Statement/s;
		}


*/

num = 5
if(num >= 10){
	console.log('Number is greater or equal to 10')
}
else{
	console.log('Number is not greater or equal to 10')
}


// Mini Activity

/*let age = 60
if(age > 59){
	console.log('Senior age')
}
else{
	console.log('Invalid age')
}*/

// notes
/*let age = parseInt(prompt("Please provide age: "));

if(age > 59){
	alert('Senior age')
}
else{
	alert('Invalid age')
}*/

/*	IF-ELSEIF-ELSE statment
	
	Syntax:
	if(condition){
		statement/s
	}
	else if(condition){
		statement/s
	}
	else if(condition){
		statements/s
	}
	.
	.
	.
	.
	else{
		statement/s
	}

	1 - Quezon City
	2 - Valenzuela City
	3 - Pasig City
	4 - Taguig City
*/

/*

let	city = parseInt(prompt("Enter a number: "));

if(city === 1){
	alert("Welcome to Quezon City");
}
else if(city === 2){
	alert("Welcome to Valenzuela City");
}
else if(city === 3){
	alert("Welcome to Pasig City");
}
else if(city === 4){
	alert("Welcome to Taguig City");
}
else {
	alert("Invalid number");
}

*/

let	message = '';

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return 'Not a typhoon yet';
	}
	else if(windSpeed <= 61){
		return 'Tropical depression detected.';
	}
	else if(windSpeed >= 61 && windSpeed <= 88){
		return 'Tropical storm detected.';
	}
	else if(windSpeed >=89 && windSpeed <= 117){
		return'severe tropical storm detected';
	}
	else{
		return'Typhoon detected.';
	}

}

message = determineTyphoonIntensity(70);
console.log(message);

// Ternary operator
/*	Syntax:
		(condition) ? ifTrue : ifFalse

	*/

let	ternaryResult = (1 < 18) ? true : false
console.log("Result of Ternary Operator: " + ternaryResult);

let	name;

function isOfLegalAge(){
	name ='John';
	return 'You are of the legal age limit';
}

function isUnderAge(){
	name ='Jane';
	return 'You are under the age limit'
}

let age = parseInt(prompt("What is your age?"));
let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge()
alert("Result of Ternary Operator in funcitons: " + legalAge + ',' + name);

// Switch statement
/* Syntax:
	switch(expression){
		case value1:
			statement/s;
			break;

		case value2:
			statement/s;
			break;

		case valuen:
			statement/s;
			break;

		default:
			statement/s;
	}
*/

let day = prompt("What day of the week is it today?").toLowerCase()

switch (day){
	case 'sunday':
		alert("The color of the day is red");
		break;
	case 'monday':
		alert("The color of the day is orange");
		break;
	case 'tuesday':
		alert("The color of the day is yellow");
		break;
	case 'wednesday':
		alert("The color of the day is green");
		break;
	case 'thursday':
		alert("The color of the day is blue");
		break;
	case 'friday':
		alert("The color of the day is indigo");
		break;
	case 'saturday':
		alert("The color of the day is violet");
		break;
	default:
		alert("please input valid day")

}

// Try-Catch-Finally Statement - commonly used for error handling

function showIntensityAlert(windSpeed){
	try{
		alerat(determineTyphoonIntensity(windSpeed));
	}
	catch (error){
		console.log(typeof error);
		console.warn(error.message);
	}
	finally{
		alert('Intensity updates will show new alert.')
	}

}

showIntensityAlert(56);